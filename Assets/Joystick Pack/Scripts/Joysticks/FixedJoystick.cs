﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class FixedJoystick : Joystick, IPointerUpHandler
{
    public override void OnPointerUp(PointerEventData eventData)
    {
        input = Vector2.zero;
        // handle.anchoredPosition = Vector2.zero;
    }
}