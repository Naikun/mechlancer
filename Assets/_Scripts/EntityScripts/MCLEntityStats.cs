﻿using UnityEngine;
public class MCLEntityStats : MonoBehaviour
{
    //Returns the Entity, Current, and total health
    public System.Action<MCLEntityStats, int, int> onDamaged;

    [SerializeField] int m_CurrentHp;
    [SerializeField] int m_TotalHp;
    public void DamageEntity(int p_damageVal)
    {
        m_CurrentHp -= m_TotalHp;


        if (onDamaged != null)
            onDamaged(this, m_CurrentHp, m_TotalHp);
    }

}
