﻿using UnityEngine;

public class MCLMechaThruster : MCLMechaComponent
{
    [SerializeField] MCLMechaThrusterData m_thrusterData;
    [SerializeField] Rigidbody m_rgdbdyToControl;

    [SerializeField] ParticleSystem m_thrustParticle;

    Vector3 m_inverseLookDirection;
    Vector3 m_lerpedForward;
    [SerializeField] float m_turnRate = 1;

    public enum ThrusterMode
    {
        Grounded,
        Aerial
    };

    [SerializeField] ThrusterMode m_thrusterMode;


    public override void Initialize()
    {
        base.Initialize();
        m_thrustParticle.Stop();
        m_thrusterData.Toggle_IsAccelerating(false);
    }
    public override void AimComponent(Vector3 p_localForward)
    {

        if (!m_thrusterData.GetIsAccelerating())
            return;

        base.AimComponent(p_localForward);


        m_lerpedForward = Vector3.Lerp(m_lerpedForward, m_setLocalForward, Time.deltaTime * m_turnRate);

        m_inverseLookDirection = p_localForward * -1;
        m_thrustParticle.transform.forward = m_inverseLookDirection;
    }


    public virtual void OverrideThrust(Vector3 p_thrust)
    {


        m_setLocalForward = p_thrust.normalized;
        m_thrusterData.OverrideCurrentStrength(p_thrust.magnitude);
        m_lerpedForward = p_thrust.normalized;
        m_thrustParticle.Play();
        m_thrustParticle.Stop();
        m_thrusterData.TryUpdate();

    }

    public override void WhileIdle()
    {
        m_thrusterData.TryUpdate();
        m_rgdbdyToControl.velocity = m_thrusterData.GetCurrentStrength() * m_lerpedForward;
    }


    public override void TryUseComponent()
    {
        if (!m_isInActiveState)
            return;

        m_thrusterData.TryUpdate();
        m_rgdbdyToControl.velocity = m_thrusterData.GetCurrentStrength() * m_lerpedForward;

    }
    public override void OnStartUsingComponent()
    {

        base.OnStartUsingComponent();

        m_thrustParticle.Play();
        m_thrusterData.Toggle_IsAccelerating(true);
    }

    public override void OnStopUsingComponent()
    {

        base.OnStopUsingComponent();

        m_thrustParticle.Stop();
        m_thrusterData.Toggle_IsAccelerating(false);
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        Gizmos.DrawLine(transform.position, transform.position + m_inverseLookDirection);

    }



    public void OnGroundedStateChanged(bool p_isGrounded)
    {
        if (p_isGrounded)
        {
            m_thrusterMode = ThrusterMode.Grounded;

        }

        else
        {
            m_thrusterMode = ThrusterMode.Aerial;
        }
    }

}
