﻿using UnityEngine;


[System.Serializable]
public class MCLMechaThrusterData
{

    [Header("[Setup]")]
    [SerializeField] float m_maxStrength = 1;
    [SerializeField] float m_accelRate = 1;
    [SerializeField] float m_decayRate = 2;

    [SerializeField] float m_currentStrength;
    [SerializeField] bool m_isAccelerating;


    public void TryUpdate()
    {
        if (m_isAccelerating)
        {
            m_currentStrength = Mathf.MoveTowards(m_currentStrength, m_maxStrength, Time.deltaTime * m_accelRate);
        }

        else
        {
            m_currentStrength = Mathf.MoveTowards(m_currentStrength, 0, Time.deltaTime * m_decayRate);

        }
    }



    public void OverrideCurrentStrength(float p_newStrength)
    {
        m_currentStrength = p_newStrength;
    }
    public void Toggle_IsAccelerating(bool p_isAccelerating)
    {
        m_isAccelerating = p_isAccelerating;
    }


    public bool GetIsAccelerating()
    {
        return m_isAccelerating;
    }

    public float GetCurrentStrength()
    {
        return m_currentStrength;
    }
}
