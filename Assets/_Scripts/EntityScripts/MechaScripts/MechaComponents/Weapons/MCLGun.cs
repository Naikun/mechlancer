﻿using System.Collections.Generic;
using UnityEngine;

public class MCLGun : MCLMechaArmWeapon
{


    [SerializeField] ParticleSystem m_fireVFX;
    [SerializeField] Transform m_projectileOrigin;

    [SerializeField] MCLGunData m_gunData;

    List<GameObject> m_spawnedProjectiles;




    public override MCLWeaponData GetWeaponData()
    {
        return m_gunData;
    }

    public override void OnActivate()
    {
        base.OnActivate();
    }

    public override void OnDeactivate()
    {
        base.OnDeactivate();
    }


    public override void Initialize()
    {
        base.Initialize();


        onWeaponReadied += TryAutoFire;


    }

    GameObject NewProjectile(GameObject p_prefabData)
    {
        GameObject spawned = Instantiate(p_prefabData, Vector3.zero, Quaternion.identity) as GameObject;

        spawned.transform.position = m_projectileOrigin.position;
        spawned.transform.forward = m_projectileOrigin.forward;

        spawned.SetActive(true);

        return spawned;
    }

    public override void AimComponent(Vector3 p_localForward)
    {
        base.AimComponent(p_localForward);


        m_fireVFX.transform.forward = p_localForward;
        m_projectileOrigin.forward = p_localForward;

        OrientSpriteToForward();
    }


    void TryAutoFire()
    {

        if (!m_isInActiveState)
            return;

        if (m_gunData.firingType != MCLGunData.FiringType.Automatic)
            return;

        OnStartUsingComponent();
    }

    public override void OnStartUsingComponent()
    {

        if (!GetWeaponData().isReadyForUse)
            return;


        if (!GetWeaponData().hasUsesRemaining)
            return;
        base.OnStartUsingComponent();

        m_fireVFX.Play();
        TryCreateProjectile();
    }

    void TryCreateProjectile()
    {
        if (m_spawnedProjectiles == null)
            m_spawnedProjectiles = new List<GameObject>();


        GameObject newProjectile = NewProjectile(m_gunData.shotPrefab);

        newProjectile.transform.parent = null;

        m_spawnedProjectiles.Add(newProjectile);


        newProjectile.transform.forward = m_projectileOrigin.forward;
        Rigidbody rgdby = newProjectile.GetComponent<Rigidbody>();


        if (rgdby != null)
            rgdby.velocity = m_projectileOrigin.forward * m_gunData.shotVelocity;
    }

}
