﻿using UnityEngine;

public class MCLWeaponData
{

    [SerializeField] protected string m_displayName;
    [SerializeField] protected Sprite m_displaySprite;

    [SerializeField] protected int m_damageVal;

    [SerializeField] int m_usesBeforeRecharge;
    [SerializeField] int m_currentUses;


    [SerializeField] bool m_isReadyForUse;
    [SerializeField] float m_delayBeforeNextUse;
    [SerializeField] float m_currentCycleDelay;

    [SerializeField] float m_delayBeforeReload;
    [SerializeField] float m_currentReloadDelay;

    public int damageVal
    {
        get
        {
            return m_damageVal;
        }
    }
    public float delayBeforeNextUse
    {
        get
        {
            return m_delayBeforeNextUse;
        }
    }

    public float delayBeforeReload
    {
        get
        {
            return m_delayBeforeReload;
        }
    }


    public float currentCycleDelay
    {
        get
        {
            return m_currentCycleDelay;
        }
    }


    public float currentReloadDelay
    {
        get
        {
            return m_currentReloadDelay;
        }
    }

    public string displayName
    {
        get
        {
            return m_displayName;
        }
    }
    public Sprite displaySprite
    {
        get
        {
            return m_displaySprite;
        }
    }
    public bool isReadyForUse
    {
        get
        {
            return m_isReadyForUse;
        }
    }

    public int usesBeforeRecharging
    {
        get
        {
            return m_usesBeforeRecharge;
        }
    }
    public int currentUses
    {
        get
        {
            return m_currentUses;
        }
    }


    public bool isCyclingWeapon
    {
        get
        {
            return m_currentCycleDelay > 0;

        }
    }


    public bool hasUsesRemaining
    {
        get
        {
            if (m_usesBeforeRecharge < 0)
                return true;

            else
            {
                if (m_currentUses > 0)
                    return true;
            }

            return false;
        }
    }
    public virtual void Initialize()
    {
        m_isReadyForUse = true;
        m_currentCycleDelay = 0;


        m_currentReloadDelay = 0;

        m_currentUses = m_usesBeforeRecharge;
    }
    public virtual void OnWeaponUsed()
    {

        //  Debug.Log("OnWeaponUsed");

        m_isReadyForUse = false;
        m_currentCycleDelay = 0;

        TryConsumeUses();

        if (!hasUsesRemaining)
        {
            //TODO: Set State to need to reload.
        }
    }



    protected virtual void TryConsumeUses()
    {
        if (m_usesBeforeRecharge <= 0)  // If something doesn't HAVE ammo, this doesn't apply.
            return;

        if (m_currentUses > 0)
        {
            m_currentUses--;
        }
    }

    public virtual void TryUpdateWeaponCycle()
    {
        if (!m_isReadyForUse)
        {
            m_currentCycleDelay += Time.deltaTime;

            if (m_currentCycleDelay >= m_delayBeforeNextUse)
            {
                m_currentCycleDelay = 0;
                m_isReadyForUse = true;
            }
        }
    }

    public virtual void TryReload()
    {
        if (!hasUsesRemaining)
        {
            m_currentReloadDelay += Time.deltaTime;

            if (m_currentReloadDelay >= m_delayBeforeReload)
            {
                m_currentReloadDelay = 0;
                m_currentUses = m_usesBeforeRecharge;
            }
        }
    }
}
