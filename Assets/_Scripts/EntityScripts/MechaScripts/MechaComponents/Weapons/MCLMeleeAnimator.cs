﻿using System.Collections;
using UnityEngine;

public class MCLMeleeAnimator : MonoBehaviour
{
    [SerializeField] Transform m_toAnimate;
    [SerializeField] Vector3 m_startScale;
    [SerializeField] Vector3 m_endScale;
    [SerializeField] float m_duration;


    IEnumerator m_activeAnim;



    public void PlayAnim()
    {
        if (m_activeAnim != null)
            StopCoroutine(m_activeAnim);

        m_activeAnim = AnimCoroutine();
        StartCoroutine(m_activeAnim);


    }

    IEnumerator AnimCoroutine()
    {
        float elapsedTime = 0;

        while (elapsedTime < m_duration)
        {
            elapsedTime += Time.deltaTime;
            Vector3 toUse = Vector3.Lerp(m_startScale, m_endScale, elapsedTime / m_duration);

            m_toAnimate.localScale = toUse;

            yield return new WaitForSeconds(Time.deltaTime);

        }
        m_toAnimate.localScale = m_endScale;

    }

}
