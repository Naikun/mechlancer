﻿using UnityEngine;

public class MCLMechaWeaponController : MCLMechaComponent
{



    public System.Action<Vector3> onWeaponControllerThrustEvent;
    public System.Action onActiveWeaponChanged;




    [SerializeField] MCLMechaArmWeapon m_leftWeapon;
    [SerializeField] MCLMechaArmWeapon m_rightWeapon;

    [SerializeField] MCLMechaWeaponMount m_leftMount;
    [SerializeField] MCLMechaWeaponMount m_rightMount;



    public override void Initialize()
    {
        base.Initialize();

        if (m_leftWeapon != null)
        {
            InitWeapon(m_leftWeapon, OnWeaponThrustEvent, m_leftMount);
        }

        if (m_rightWeapon != null)
        {
            InitWeapon(m_rightWeapon, OnWeaponThrustEvent, m_rightMount);
        }
    }



    void InitWeapon(MCLMechaArmWeapon p_weapon, System.Action<Vector3> onThrustEvent, MCLMechaWeaponMount p_mount)
    {
        p_weapon.Initialize();
        p_weapon.onThrustEvent = onThrustEvent;
        p_mount.SetArmWeapon(p_weapon);
    }


    public void NextWeapon()
    {
        // SetWeaponIndex(m_activeWeaponIndex + 1);
    }

    public void PreviousWeapon()
    {

        //  SetWeaponIndex(m_activeWeaponIndex - 1);
    }


    void OnWeaponThrustEvent(Vector3 p_thrust)
    {
        if (onWeaponControllerThrustEvent != null)
            onWeaponControllerThrustEvent(p_thrust);
    }


    public override void WhileUsingComponent()
    {
        if (m_leftWeapon != null)
            m_leftWeapon.WhileUsingComponent();

        if (m_rightWeapon != null)
            m_rightWeapon.WhileUsingComponent();
    }

    public override void WhileIdle()
    {


        if (m_leftWeapon != null)
        {
            m_leftWeapon.TryUpdate_WeaponCycle();
            m_leftWeapon.TryUpdate_WeaponReload();
            m_leftWeapon.WhileIdle();
        }

        if (m_rightWeapon != null)
        {
            m_rightWeapon.TryUpdate_WeaponCycle();
            m_rightWeapon.TryUpdate_WeaponReload();
            m_rightWeapon.WhileIdle();
        }
    }

    public override void OnStartUsingComponent()
    {

        if (m_leftWeapon != null)
        {
            m_leftWeapon.OnStartUsingComponent();
        }

        if (m_rightWeapon != null)
        {
            m_rightWeapon.OnStartUsingComponent();
        }

    }

    public override void OnStopUsingComponent()
    {

        if (m_leftWeapon != null)
        {
            m_leftWeapon.OnStopUsingComponent();
        }

        if (m_rightWeapon != null)
        {
            m_rightWeapon.OnStopUsingComponent();
        }
    }

    public override void AimComponent(Vector3 p_localForward)
    {
        if (m_leftWeapon != null)
        {
            m_leftWeapon.AimComponent(p_localForward);
        }

        if (m_rightWeapon != null)
        {
            m_rightWeapon.AimComponent(p_localForward);
        }
    }

}
