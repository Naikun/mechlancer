﻿using UnityEngine;

public class MCLMechaArmWeapon : MCLMechaComponent
{

    public System.Action<Vector3> onThrustEvent;
    public System.Action onWeaponReadied;


    [SerializeField] GameObject m_rendererObject;


    public override void Initialize()
    {
        base.Initialize();
        GetWeaponData().Initialize();



    }
    public virtual MCLWeaponData GetWeaponData()
    {
        return null;
    }
    protected virtual void OrientSpriteToForward()
    {
        m_rendererObject.transform.right = -m_setLocalForward;

    }


    public override void WhileIdle()
    {
        if (m_isInActiveState)
            return;

    }

    public virtual void TryUpdate_WeaponCycle()
    {
        GetWeaponData().TryUpdateWeaponCycle();

        if (GetWeaponData().isReadyForUse)
        {

            if (onWeaponReadied != null)
                onWeaponReadied();
        }

    }

    public virtual void TryUpdate_WeaponReload()
    {
        GetWeaponData().TryReload();

        if (GetWeaponData().isReadyForUse)
        {

            if (onWeaponReadied != null)
                onWeaponReadied();
        }
    }

    public virtual void OnActivate()
    {

        m_rendererObject.SetActive(true);
    }

    public virtual void OnDeactivate()
    {
        m_rendererObject.SetActive(false);
    }

    public override void OnStartUsingComponent()
    {
        base.OnStartUsingComponent();

        /*
            if (!GetWeaponData().isReadyForUse)
            return;


            if(!GetWeaponData().hasUsesRemaining)
            return;
            */



        GetWeaponData().OnWeaponUsed();


    }
}
