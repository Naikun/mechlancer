﻿using UnityEngine;

[System.Serializable]
public class MCLMeleeData : MCLWeaponData
{

    [SerializeField] float m_meleeRadius = 1.0f;
    [SerializeField] float m_forwardVelocity;
    [SerializeField] GameObject[] m_slashPrefabs;




    public GameObject[] slashPrefabs
    {
        get
        {
            return m_slashPrefabs;
        }
    }
    public float meleeRadius
    {
        get
        {
            return m_meleeRadius;
        }
    }

    public float forwardVelocity => m_forwardVelocity;

}
