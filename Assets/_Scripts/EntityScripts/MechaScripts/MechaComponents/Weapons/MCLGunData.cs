﻿using UnityEngine;


[System.Serializable]
public class MCLGunData : MCLWeaponData


{


    public enum FiringType
    {
        SingleShot,
        Automatic
    }
    [SerializeField] FiringType m_firingType;


    [SerializeField] GameObject m_shotPrefab;
    [SerializeField] float m_shotVelocity;
    [SerializeField] float m_shotSpread;
    [SerializeField] float m_recoil;

    public FiringType firingType => m_firingType;

    public GameObject shotPrefab
    {
        get
        {
            return m_shotPrefab;
        }
    }
    public float shotVelocity
    {
        get
        {
            return m_shotVelocity;
        }
    }
    public float shotSpread
    {
        get
        {
            return m_shotSpread;
        }
    }

    public float recoil => m_recoil;


}
