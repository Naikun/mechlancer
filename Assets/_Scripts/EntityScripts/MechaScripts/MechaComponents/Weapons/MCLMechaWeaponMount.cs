﻿using UnityEngine;

public class MCLMechaWeaponMount : MCLMechaComponent
{
    [SerializeField] MCLMechaArmWeapon m_assignedArmWeapon;

    public MCLMechaArmWeapon assignedArmWeapon => m_assignedArmWeapon;

    public void SetArmWeapon(MCLMechaArmWeapon p_armWeapon)
    {
        m_assignedArmWeapon = p_armWeapon;
    }
}
