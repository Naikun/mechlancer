﻿using UnityEngine;

public class MCLMelee : MCLMechaArmWeapon
{


    const float SLASH_FX_OFFSET = 1.75f;
    [SerializeField] MCLMeleeData m_meleeData;

    [SerializeField] ParticleSystem m_onSlashFX;
    [SerializeField] MCLMeleeAnimator m_animator;


    Vector3 m_lastKnownMeleePosition;

    public override void Initialize()
    {
        base.Initialize();
        m_onSlashFX.transform.parent = null;
    }

    public override MCLWeaponData GetWeaponData()
    {
        return m_meleeData;
    }

    public override void AimComponent(Vector3 p_localForward)
    {
        base.AimComponent(p_localForward);

        OrientSpriteToForward();
    }


    public override void OnStartUsingComponent()
    {


        if (!GetWeaponData().isReadyForUse)
            return;


        if (!GetWeaponData().hasUsesRemaining)
            return;

        base.OnStartUsingComponent();


        m_onSlashFX.transform.right = m_setLocalForward; // This orientation set is asset Specific. The asset being used specifically animates in a Downward Slash, so this is the alignment fix. 
        m_onSlashFX.transform.position = transform.position + m_setLocalForward * SLASH_FX_OFFSET;  // This is largely by feel. 

        m_onSlashFX.Play();
        m_animator.PlayAnim();
        m_meleeData.OnWeaponUsed();

        TryCalc_MeleeHits();


        if (onThrustEvent != null)
            onThrustEvent(m_setLocalForward * m_meleeData.forwardVelocity);
    }


    void TryCalc_MeleeHits()
    {
        m_lastKnownMeleePosition = transform.position + m_setLocalForward;


    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        Gizmos.DrawWireSphere(m_lastKnownMeleePosition, m_meleeData.meleeRadius);

    }
}
