﻿using System.Collections;
using UnityEngine;

public class MCLMechaEvader : MCLMechaComponent
{
    [SerializeField] MCLMechaAfterImager m_afterimager;
    [SerializeField] SpriteRenderer[] m_toAnimate;


    [SerializeField] Color m_noEvadeCol = Color.clear;
    [SerializeField] Color m_hasEvadeCol = Color.black;

    [SerializeField] float m_evasionStartup = 0.1f;
    [SerializeField] float m_evasionDuration = 0.2f;
    [SerializeField] float m_evasionFollowThrough = 0.05f;


    IEnumerator m_activeCoroutine;

    Color GetEvasionColor(float p_f)
    {
        return Color.Lerp(m_noEvadeCol, m_hasEvadeCol, p_f);
    }


    public override void Initialize()
    {
        base.Initialize();
        m_afterimager.Initialize();
    }



    void TryNew_ActiveCoroutine()
    {
        if (m_activeCoroutine != null)
            StopCoroutine(m_activeCoroutine);

        m_activeCoroutine = RotateAnimCoroutine();

        StartCoroutine(m_activeCoroutine);
    }

    IEnumerator RotateAnimCoroutine()
    {
        float elapsedTime = 0;
        float lerpStep = 0;
        float durationToWatch = m_evasionStartup;

        while (elapsedTime < durationToWatch)
        {
            elapsedTime += Time.deltaTime;
            lerpStep = elapsedTime / m_evasionStartup;

            TrySetEvasionColor(GetEvasionColor(lerpStep));
            yield return new WaitForSeconds(Time.deltaTime);
        }


        yield return new WaitForSeconds(m_evasionDuration);


        elapsedTime = 0;
        lerpStep = 0;
        durationToWatch = m_evasionFollowThrough;
        while (elapsedTime < durationToWatch)
        {
            elapsedTime += Time.deltaTime;
            lerpStep = 1 - (elapsedTime / m_evasionStartup);

            TrySetEvasionColor(GetEvasionColor(lerpStep));
            yield return new WaitForSeconds(Time.deltaTime);
        }



        while (elapsedTime < durationToWatch)
        {
            elapsedTime += Time.deltaTime;
            lerpStep = elapsedTime / m_evasionStartup;

            TrySetEvasionColor(GetEvasionColor(lerpStep));
            yield return new WaitForSeconds(Time.deltaTime);
        }

        TrySetEvasionColor(m_noEvadeCol);



    }

    void TrySetEvasionColor(Color p_col)
    {
        for (int i = 0; i < m_toAnimate.Length; i++)
        {
            m_toAnimate[i].material.SetColor("_Color", p_col);
        }
    }



    [Sirenix.OdinInspector.Button]
    public override void OnStartUsingComponent()
    {
        base.OnStartUsingComponent();
        TryNew_ActiveCoroutine();
        m_afterimager.PlayAfterimages();
    }

}
