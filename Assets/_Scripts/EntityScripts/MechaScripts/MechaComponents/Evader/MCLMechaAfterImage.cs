﻿using System.Collections;
using UnityEngine;


public class MCLMechaAfterImage : MonoBehaviour
{
    [SerializeField] SpriteRenderer m_sr;
    [SerializeField] float m_instanceDuration;

    [SerializeField] Vector3 m_startScale;
    [SerializeField] Vector3 m_endScale;


    [SerializeField] Transform m_host;
    [SerializeField] Color m_startCol;
    [SerializeField] Color m_endCol;
    [SerializeField] float m_delayBeforeSpawn;


    IEnumerator m_activeCoroutine;


    Vector3 GetPosDuringAnimation()
    {
        return m_host.position;
    }

    public void SetDelayBeforeSpawn(float p_distFactor)
    {
        m_delayBeforeSpawn = p_distFactor;
    }
    public void SetHost(Transform p_host)
    {
        m_host = p_host;
    }

    public void SetScale(Vector3 p_scale)
    {
        transform.localScale = p_scale;
    }

    public void SetColors(Color p_startCol, Color p_endCol)
    {
        m_startCol = p_startCol;
        m_endCol = p_endCol;
    }
    public void SetSprite(Sprite p_sprite)
    {
        m_sr.sprite = p_sprite;
    }

    public void SetTransform(Vector3 p_pos, Quaternion p_rot)
    {
        transform.position = p_pos;
        transform.rotation = p_rot;
    }

    public void SetMatCol(Color p_col)
    {
        m_sr.sharedMaterial.SetColor("_Color", p_col);
        m_sr.color = p_col;
    }


    public void PlayAfterimages()
    {

        // Debug.Log("[AfterImager PLAY!]");
        if (m_activeCoroutine != null)
            StopCoroutine(m_activeCoroutine);

        m_activeCoroutine = ActiveCoroutine();
        StartCoroutine(m_activeCoroutine);
    }

    IEnumerator ActiveCoroutine()
    {
        yield return new WaitForSeconds(m_delayBeforeSpawn);

        float elapsedTime = 0;
        float lerpStep = elapsedTime / m_instanceDuration;


        SetTransform(m_host.position, m_host.rotation);

        while (elapsedTime < m_instanceDuration)
        {
            elapsedTime += Time.deltaTime;
            lerpStep = elapsedTime / m_instanceDuration;


            yield return new WaitForSeconds(Time.deltaTime * 2);


            Color toUse = Color.Lerp(m_startCol, m_endCol, lerpStep);
            Vector3 scaleToUse = Vector3.Lerp(m_startScale, m_endScale, lerpStep);


            SetScale(scaleToUse);
            SetMatCol(toUse);
        }

        // Debug.Log("[AfterImager FINISH!]");

    }


}
