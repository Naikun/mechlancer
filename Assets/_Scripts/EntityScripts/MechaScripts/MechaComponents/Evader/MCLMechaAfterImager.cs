﻿using UnityEngine;


public class MCLMechaAfterImager : MonoBehaviour
{
    [SerializeField] int m_totalAfterimages;  //How many Afterimages we're going to spawn    
    [SerializeField] float m_delayPerImage;   // How long each image delays before it tries to match the host position and orientation.
    [SerializeField] float m_afterimageDuration; //Length of effect total;
    [SerializeField] float m_distanceFromHost; //How far away is each afterimage;
    [SerializeField] Sprite m_spriteToUse;      // Which sprite we display

    [SerializeField] MCLMechaAfterImage m_instanceToSpawn;  // Prefabbing
    [SerializeField] MCLMechaAfterImage[] m_instances;  // Prefabbing



    [SerializeField] Transform m_hostTransform;
    [SerializeField] Color m_startCol = Color.white;
    [SerializeField] Color m_endCol = Color.white;




    Color GetStartCol(int p_indexOf)
    {
        return Color.Lerp(m_startCol, m_endCol, (float)(m_totalAfterimages - p_indexOf / m_totalAfterimages));
    }

    MCLMechaAfterImage CreateNew()
    {
        MCLMechaAfterImage toReturn = null;


        GameObject g = Instantiate(m_instanceToSpawn.gameObject, Vector3.zero, Quaternion.identity) as GameObject;
        toReturn = g.GetComponent<MCLMechaAfterImage>();

        return toReturn;
    }



    void ResetAfterimages()
    {
        for (int i = 0; i < m_totalAfterimages; i++)
        {
            m_instances[i].SetMatCol(Color.clear);
            m_instances[i].SetSprite(m_spriteToUse);

        }
    }
    void PrebakeAfterimages()
    {
        m_instances = new MCLMechaAfterImage[m_totalAfterimages];

        for (int i = 0; i < m_totalAfterimages; i++)
        {
            m_instances[i] = CreateNew();
        }

        ResetAfterimages();
    }


    public void Initialize()
    {
        // Debug.Log("[AfterImager] INITIALZIE");
        PrebakeAfterimages();
    }



    public void PlayAfterimages()
    {

        for (int i = 0; i < m_instances.Length; i++)
        {
            float hostDistFactor = 0.1f * (i);
            m_instances[i].SetDelayBeforeSpawn(hostDistFactor);
            m_instances[i].SetColors(m_startCol, m_endCol);
            m_instances[i].SetHost(m_hostTransform);

            m_instances[i].SetTransform(m_hostTransform.position, m_hostTransform.rotation);

            m_instances[i].PlayAfterimages();

        }


    }





}
