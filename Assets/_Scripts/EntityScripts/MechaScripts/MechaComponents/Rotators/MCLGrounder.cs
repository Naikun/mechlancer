﻿using UnityEngine;

public class MCLGrounder : MCLMechaComponent
{


    public System.Action<bool> onGroundedStateChanged;
    [SerializeField] bool m_isGrounded;

    private void OnCollisionEnter(UnityEngine.Collision collision)
    {
        Debug.Log("[HeyIHitSomething] " + collision.gameObject.name);


        if (collision.gameObject.tag == "Floor")
        {


            if (!m_isGrounded)
            {
                m_isGrounded = true;

                if (onGroundedStateChanged != null)
                    onGroundedStateChanged(m_isGrounded);
            }
        }
    }

    private void OnCollisionExit(UnityEngine.Collision collision)
    {
        Debug.Log("[ImOutPeace] " + collision.gameObject.name);
        if (collision.gameObject.tag == "Floor")
        {
            if (m_isGrounded)
            {
                m_isGrounded = false;

                if (onGroundedStateChanged != null)
                    onGroundedStateChanged(m_isGrounded);
            }

        }
    }
}
