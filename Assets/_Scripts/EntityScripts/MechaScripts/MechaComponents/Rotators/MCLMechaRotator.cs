﻿using UnityEngine;

public class MCLMechaRotator : MCLMechaComponent
{




    [SerializeField] Transform m_bodyToRotate;
    [SerializeField] Transform m_weaponToRotate;
    [SerializeField] float m_bodyRotateRate = 1.0f;

    public enum DefaultFacing

    {
        Left,
        Right
    };

    public enum RotatableMode
    {
        Grounded,
        Aerial
    };

    [SerializeField] DefaultFacing m_defaultFacing;
    [SerializeField] RotatableMode m_rotateMode;

    [SerializeField] SpriteRenderer[] m_bodySprites;
    [SerializeField] SpriteRenderer[] m_weaponSprites;


    SpriteRenderer[] m_allSprites;


    DefaultFacing GetOppositeFacing()
    {
        if (m_defaultFacing == DefaultFacing.Left)
            return DefaultFacing.Right;

        else
            return DefaultFacing.Left;
    }


    Vector3 GetOrientationFacingVector()
    {
        switch (m_defaultFacing)
        {
            case DefaultFacing.Left:
            return -Vector3.right;

            case DefaultFacing.Right:
            return Vector3.right;
        }


        return Vector3.zero;
    }


    float DotProductToFacingVector(Vector3 p_newOrientation)
    {
        return Vector3.Dot(GetOrientationFacingVector(), p_newOrientation);
    }

    public override void Initialize()
    {
        base.Initialize();

        InitAllSprites();
    }


    void InitAllSprites()
    {
        m_allSprites = new SpriteRenderer[m_bodySprites.Length + m_weaponSprites.Length];

        for (int i = 0; i < m_bodySprites.Length; i++)
        {
            m_allSprites[i] = m_bodySprites[i];
        }
        for (int i = m_bodySprites.Length; i < m_bodySprites.Length + m_weaponSprites.Length; i++)
        {
            int indexToUse = i - m_bodySprites.Length;
            m_allSprites[i] = m_weaponSprites[indexToUse];
        }
    }
    void SetSpriteRendererFacing(DefaultFacing p_facing)
    {
        if (m_rotateMode == RotatableMode.Aerial)
        {

            for (int i = 0; i < m_allSprites.Length; i++)
            {
                m_allSprites[i].flipX = false;
                m_allSprites[i].flipY = (p_facing != m_defaultFacing);
            }
        }


        if (m_rotateMode == RotatableMode.Grounded)
        {

            for (int i = 0; i < m_bodySprites.Length; i++)
            {
                m_bodySprites[i].flipY = false;
                m_bodySprites[i].flipX = (p_facing != m_defaultFacing);
            }

            for (int i = 0; i < m_weaponSprites.Length; i++)
            {
                m_weaponSprites[i].flipX = false;
                m_weaponSprites[i].flipY = (p_facing != m_defaultFacing);
            }
        }
    }

    void MatchFacingScale_ToOrientation_Body(Vector3 p_localForward)
    {
        float dot = DotProductToFacingVector(p_localForward);


        if (dot > 0)
            SetSpriteRendererFacing(m_defaultFacing);

        else
            SetSpriteRendererFacing(GetOppositeFacing());
    }

    void Match_RendererOrientation_ToOrentation(Vector3 p_localForward)
    {
        Vector3 rightOrientation = Vector3.Lerp(m_bodyToRotate.right, p_localForward, m_bodyRotateRate * Time.deltaTime);

        if (m_rotateMode == RotatableMode.Grounded)
        {
            rightOrientation = -GetOrientationFacingVector();
        }

        m_bodyToRotate.right = rightOrientation;
    }


    public override void AimComponent(Vector3 p_localForward)
    {
        base.AimComponent(p_localForward);
        Match_RendererOrientation_ToOrentation(-p_localForward);
        MatchFacingScale_ToOrientation_Body(p_localForward);
        //  MatchFacingScale_ToOrientation_Weapon(p_localForward);
        // Debug.Log("[AimVector DotProduct with Current facing]" + DotProductToFacingVector(p_localForward)); 
    }


    public void OnGroundedStateChanged(bool p_isGrounded)
    {
        if (p_isGrounded)
        {
            m_rotateMode = RotatableMode.Grounded;
        }

        else
        {
            m_rotateMode = RotatableMode.Aerial;
        }
    }








}
