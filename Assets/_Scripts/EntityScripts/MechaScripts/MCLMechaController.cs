﻿using UnityEngine;

public class MCLMechaController : MonoBehaviour
{

    const string INPUT_TYPE_THRUSTER = "Thrust";
    const string INPUT_TYPE_FIRE = "Fire";
    const string INPUT_TYPE_NEXTWEAPON = "NextWeapon";
    const string INPUT_TYPE_PREVIOUSWEAPON = "PreviousWeapon";

    const string INPUT_TYPE_EVADE = "Evade";

    [Header("[Thrusters]")]
    [SerializeField] MCLMechaThruster m_thrusterMain;
    [SerializeField] MCLMechaRotator m_rotator;
    [SerializeField] MCLGrounder m_grounder;

    [Header("[Weapons]")]
    [SerializeField] MCLMechaWeaponController m_weaponController;
    [SerializeField] MCLMechaEvader m_evader;

    [SerializeField] MCLEntityStats m_entityStats;



    MCLMechaComponent[] m_components;



    public MCLMechaWeaponController weaponController
    {
        get
        {
            return m_weaponController;
        }
    }

    Vector2 MousePosToLookAngle(Vector2 p_mousePos)
    {
        Vector3 dir_toMouse = Vector3.zero;
        Vector3 myPosToScreen = Camera.main.WorldToScreenPoint(transform.position);


        dir_toMouse = myPosToScreen - Input.mousePosition;
        dir_toMouse.z = 0;
        dir_toMouse.Normalize();

        return -dir_toMouse;
    }



    public virtual void TryAimComponents(Vector3 p_aimvector)
    {
        for (int i = 0; i < m_components.Length; i++)
        {
            m_components[i].AimComponent(p_aimvector);
        }
    }



    public virtual void TryUseComponents()
    {
        for (int i = 0; i < m_components.Length; i++)
        {
            m_components[i].TryUseComponent();
        }
    }


    public virtual void TryIdleComponents()
    {
        for (int i = 0; i < m_components.Length; i++)
        {
            m_components[i].WhileIdle();
        }
    }

    void Try_AimComponents()
    {
        Vector2 activeDirection = MCLInputManagerDirectional.GetInstance().GetInputAxis();


        if (MCLInputManagerDirectional.GetInstance().directionalInputMethod == DirectionalInputMethod.Mouse)
            activeDirection = MousePosToLookAngle(MCLInputManagerDirectional.GetInstance().GetInputAxis());


        TryAimComponents(activeDirection);
    }

    public void TryOverrideThrust(Vector3 p_thrustVec)
    {
        m_thrusterMain.OverrideThrust(p_thrustVec);
    }





    public virtual void Initialize()
    {
        m_thrusterMain.Initialize();

        m_weaponController.Initialize();
        m_weaponController.onWeaponControllerThrustEvent = TryOverrideThrust;

        m_evader.Initialize();
        m_rotator.Initialize();
        m_grounder.Initialize();



        m_components = new MCLMechaComponent[4];

        m_components[0] = m_thrusterMain;
        m_components[1] = m_weaponController;
        m_components[2] = m_rotator;
        m_components[3] = m_evader;



        m_grounder.onGroundedStateChanged += m_rotator.OnGroundedStateChanged;
        m_grounder.onGroundedStateChanged += m_thrusterMain.OnGroundedStateChanged;


        //Setup Input Callbacks to binary Input 

        //Thrusters
        MCLInputManagerBinary.GetInstance().GetInputType(INPUT_TYPE_THRUSTER).onInputDown += m_thrusterMain.OnStartUsingComponent;
        MCLInputManagerBinary.GetInstance().GetInputType(INPUT_TYPE_THRUSTER).onInputUp += m_thrusterMain.OnStopUsingComponent;
        //Weapon Activation
        MCLInputManagerBinary.GetInstance().GetInputType(INPUT_TYPE_FIRE).onInputDown += m_weaponController.OnStartUsingComponent;
        MCLInputManagerBinary.GetInstance().GetInputType(INPUT_TYPE_FIRE).onInputUp += m_weaponController.OnStopUsingComponent;

        //Weapon Cycle Next/Previous
        MCLInputManagerBinary.GetInstance().GetInputType(INPUT_TYPE_NEXTWEAPON).onInputDown += m_weaponController.NextWeapon;
        MCLInputManagerBinary.GetInstance().GetInputType(INPUT_TYPE_PREVIOUSWEAPON).onInputDown += m_weaponController.PreviousWeapon;

        //Evasion
        MCLInputManagerBinary.GetInstance().GetInputType(INPUT_TYPE_EVADE).onInputDown += m_evader.OnStartUsingComponent;
        MCLInputManagerBinary.GetInstance().GetInputType(INPUT_TYPE_EVADE).onInputUp += m_evader.OnStopUsingComponent;
    }




    private void Update()
    {
        if (m_components == null)
            return;

        if (m_components.Length < 1)
            return;

        Try_AimComponents();
        TryUseComponents();
        TryIdleComponents();
    }



}
