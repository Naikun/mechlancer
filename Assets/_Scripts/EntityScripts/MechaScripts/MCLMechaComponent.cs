﻿
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class MCLMechaComponent : MonoBehaviour
{

    [SerializeField] protected  Color m_debugColor = Color.white;
    [SerializeField] protected bool m_debug_showDirection = true;

    [SerializeField] protected Vector3 m_setLocalForward;
    [SerializeField] protected bool m_isInActiveState;


    public virtual void Initialize()
    {
    }

    public virtual void AimComponent(Vector3 p_localForward)
    {
        m_setLocalForward = p_localForward;
    }


    public virtual void WhileIdle()
    {
     
    }
    public virtual void WhileUsingComponent()
    {

    }



    public virtual void OnStartUsingComponent()
    {
        
        m_isInActiveState = true;
    }
    public virtual void OnStopUsingComponent()
    {
        m_isInActiveState = false;
    }

    public virtual void TryUseComponent()
    {
        if (!m_isInActiveState)
            return;
    }

    protected virtual void OnDrawGizmos()
    {
        if (!m_debug_showDirection)
            return;

            Gizmos.color = m_debugColor;
 
            Gizmos.DrawLine(transform.position, transform.position + m_setLocalForward);
    }
}
