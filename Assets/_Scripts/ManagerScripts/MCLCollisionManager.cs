﻿using System.Collections.Generic;
using UnityEngine;

public class MCLCollisionManager : MonoBehaviour
{
    [SerializeField] MCLCollideableEntity[] m_collideableEntities;

    public void RefreshCollideables()
    {
        m_collideableEntities = FindObjectsOfType<MCLCollideableEntity>();
    }



    public MCLCollideableEntity[] GetCollisions(float p_radius, Vector3 p_position)
    {

        List<MCLCollideableEntity> collisions = new List<MCLCollideableEntity>();


        for (int i = 0; i < m_collideableEntities.Length; i++)
        {
            float dist = Vector3.Distance(m_collideableEntities[i].transform.position, p_position);


            if (dist < p_radius)
                collisions.Add(m_collideableEntities[i]);
        }

        return collisions.ToArray();


    }
}
