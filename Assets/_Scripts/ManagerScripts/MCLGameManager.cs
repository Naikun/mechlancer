﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCLGameManager : MonoBehaviour
{
    [SerializeField] MCLMechaController m_playerMechaController;
    [SerializeField] MCLMechaWeaponGroup m_weaponGroupUI;

    private void Start  ()
    {
        Initialize();
    }

    private void Initialize()
    {
        m_playerMechaController.Initialize();
        m_weaponGroupUI.Initialize(m_playerMechaController.weaponController);
    }
}
