﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class MCLMechaWeaponDisplay : MonoBehaviour
{




float  SCALE_ACTIVE = 1.0f;
float SCALE_INACTIVE = 0.75f;

Color COLOR_ACTIVE = Color.white;
Color COLOR_INACTIVE = Color.gray;


    MCLMechaArmWeapon m_assignedWeapon;
        public MCLMechaArmWeapon assignedWeapon { get { return m_assignedWeapon;}}


    [Header("[Objects]")]
    [SerializeField] Text m_weaponName;
    [SerializeField] Text m_ammoCount;
    [SerializeField] Image m_fillImage;

    [SerializeField] Image m_overlayImage;
    [SerializeField] Image m_bgImage;
    [SerializeField] Image m_iconImage;



    MCLWeaponData GetWeaponData()
    {
        if (m_assignedWeapon == null)
            return null;

        return m_assignedWeapon.GetWeaponData();
    }

    float GetWeaponCycleProgress()
    {
        if (m_assignedWeapon == null)
            return 0;

        else return GetWeaponData().delayBeforeNextUse;
        
    }

    [Sirenix.OdinInspector.Button]
    public void InitAsWeapon(MCLMechaArmWeapon p_weaponInstance)
    {
        m_assignedWeapon = p_weaponInstance;

        UpdateVisuals();

        
    }


    public void UpdateVisuals()
    {
        if (GetWeaponData() == null)
            return;

        TryUpdate_BaseVisuals();
        TryUpdate_Recycle();
        TryUpdate_Ammo();
    }


    void TryUpdate_BaseVisuals()
    {
            m_weaponName.text = GetWeaponData().displayName;
            m_iconImage.sprite = GetWeaponData().displaySprite;
        
    }
    void TryUpdate_Recycle()
    {
        //m_fillImage.fillAmount =  GetWeaponData().currentCycleDelay/ GetWeaponData().delayBeforeNextUse;
          m_fillImage.fillAmount =  GetWeaponData().currentReloadDelay/ GetWeaponData().delayBeforeReload;
    }

    void TryUpdate_Ammo()
    {
        if(GetWeaponData().usesBeforeRecharging <= 0)
        m_ammoCount.text = "INF";

        else
        m_ammoCount.text = GetWeaponData().currentUses.ToString()  + "/"+ GetWeaponData().usesBeforeRecharging.ToString();
    }

    public virtual void SetAsActive(bool p_active)
    {
      m_overlayImage.enabled = !p_active;
      transform.localScale = p_active ? SCALE_ACTIVE * Vector3.one : SCALE_INACTIVE * Vector3.one;


      if(p_active)
      transform.SetAsFirstSibling();
      
    }
}
