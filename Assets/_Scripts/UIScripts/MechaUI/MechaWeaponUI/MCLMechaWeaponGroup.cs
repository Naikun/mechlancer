﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCLMechaWeaponGroup : MonoBehaviour
{
    [SerializeField] MCLMechaArmWeapon[] m_toInitializeFrom;

    [SerializeField] MCLMechaWeaponDisplay[] m_instances;
    [SerializeField] MCLMechaWeaponDisplay m_prefab;
    [SerializeField] Transform m_parentTransform;



    MCLMechaWeaponController m_weaponController;
    MCLMechaWeaponDisplay NewInstance(MCLMechaArmWeapon p_weapon)
    {
        GameObject g = Instantiate(m_prefab.gameObject, m_parentTransform) as GameObject;

        MCLMechaWeaponDisplay toReturn = g.GetComponent<MCLMechaWeaponDisplay>();
        toReturn.InitAsWeapon(p_weapon);

        return toReturn;
    }

    public void SetWeapons(MCLMechaArmWeapon[] p_weapons)
    {
        m_toInitializeFrom = p_weapons;
    }

    public void Initialize(MCLMechaWeaponController p_weaponController)
    {
        m_weaponController = p_weaponController;
        SetWeapons(m_weaponController.weapons);
        TryCreate_Instances();
        TrySet_ActiveInstance();


        p_weaponController.onActiveWeaponChanged = OnActiveWeaponChanged;
    }


    void OnActiveWeaponChanged()
    {
        TrySet_ActiveInstance();

        Debug.Log("OnActievWeaponChanged UI");
    }



    void TryCreate_Instances()
    {
        m_instances = new MCLMechaWeaponDisplay[m_toInitializeFrom.Length];

        for (int i = 0; i < m_toInitializeFrom.Length; i++)
        {
            m_instances[i] = NewInstance(m_toInitializeFrom[i]);
        }
    }

    void TrySet_ActiveInstance()
    {
        for (int i = 0; i < m_instances.Length; i++)
        {
          m_instances[i].SetAsActive(m_instances[i].assignedWeapon ==  m_weaponController.GetActiveWeapon());
        }
    }

    void TryUpdate_Instances()
    {
       for (int i = 0; i < m_instances.Length; i++)
        {
          m_instances[i].UpdateVisuals();
        }
    }

    private void Update()
    {
     TryUpdate_Instances();
        
    }

}
