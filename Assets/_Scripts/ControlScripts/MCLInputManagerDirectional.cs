﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCLInputManagerDirectional : MonoBehaviour
{



    public static MCLInputManagerDirectional GetInstance()
    {
        if (!m_instance)
            m_instance = FindObjectOfType<MCLInputManagerDirectional>();

        return m_instance;
    }
    static MCLInputManagerDirectional m_instance;

    public DirectionalInputMethod directionalInputMethod
    {
        get
        {
            return m_directionalInputMethod;
        }
    }

    [SerializeField] DirectionalInputMethod m_directionalInputMethod;
    [SerializeField] float m_horizontalAxis;
    [SerializeField] float m_verticalAxis;


    [Header("[Optional Fields]")]
    [SerializeField] Joystick m_joystick; // Replace when joystick is integrated.


    [SerializeField] Vector2 m_inputAxis;

    public Vector2 GetInputAxis()
    {
        return m_inputAxis;
    }



    void UpdateInputAxis()
    {
        switch (m_directionalInputMethod)
        {
            case DirectionalInputMethod.Joystick:

                if (m_joystick != null)
                {
                    m_horizontalAxis = m_joystick.Direction.x;
                    m_verticalAxis = m_joystick.Direction.y;
                }

                break;

            case DirectionalInputMethod.KeyboardAxis:
                m_horizontalAxis = Input.GetAxis("Horizontal");
                m_verticalAxis = Input.GetAxis("Vertical");
                break;

            case DirectionalInputMethod.Mouse:
                m_horizontalAxis = Input.mousePosition.x;
                m_verticalAxis = Input.mousePosition.y;
                break;
        }

        m_inputAxis.x = m_horizontalAxis;
        m_inputAxis.y = m_verticalAxis;
    }



    void Try_AutoDetermineInput()
    {
        #if UNITY_ANDROID
        m_directionalInputMethod = DirectionalInputMethod.Joystick;
        Debug.Log("[Android Build detected! Overrriding to use JOystick");
        return;
        #endif


      

        m_directionalInputMethod = DirectionalInputMethod.Mouse;
    }

    private void Start()
    {
        Try_AutoDetermineInput();
    }
    private void Update()
    {
        UpdateInputAxis();
    }

}
