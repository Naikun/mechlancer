﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class MCLBinaryInputData 
{

    public System.Action onInputDown;
    public System.Action onInputUp;


    [SerializeField] string m_inputDescription;
    [SerializeField] BinaryInputMethod m_inputMethod;

    [ShowIf("isKeyboard")] [SerializeField] KeyCode m_keyCode;
    [ShowIf("isMouseButton")] [SerializeField] int m_mouseButtonIndex;


    public string inputDescription { get { return m_inputDescription; } }
    public BinaryInputMethod inputMethod { get { return m_inputMethod; } }

    public bool isKeyboard { get { return m_inputMethod == BinaryInputMethod.KeyCode; } }
    public bool isMouseButton { get { return m_inputMethod == BinaryInputMethod.Mousebutton; } }

  

    public KeyCode keyCode { get { return m_keyCode; } }
  public int mouseButtonIndex { get { return m_mouseButtonIndex; } }




  public void OnUIInputDown()
  {
    if (onInputDown != null)
                    onInputDown();
  }

  public void OnUIInputUp()
  {
   if (onInputUp != null)
                    onInputUp();
  }

    public void OnBinaryInputDown()
    {


        if (isKeyboard)
        {
            if (Input.GetKeyDown(m_keyCode))
            {
                if (onInputDown != null)
                    onInputDown();
            }
        }


        if (isMouseButton)
        {
            if (Input.GetMouseButtonDown(m_mouseButtonIndex))
            {
                if (onInputDown != null)
                    onInputDown();
            }
        }

    }

    public void OnBinaryInputUp()
    {


        if (isKeyboard)
        {
            if (Input.GetKeyUp(m_keyCode))
            {
                if (onInputUp != null)
                    onInputUp();
            }
        }


        if (isMouseButton)
        {
            if (Input.GetMouseButtonUp(m_mouseButtonIndex))
            {
                if (onInputUp != null)
                    onInputUp();
            }
        }
    }

}
