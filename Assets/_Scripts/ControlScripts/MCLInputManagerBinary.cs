﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MCLInputManagerBinary : MonoBehaviour
{


    public static MCLInputManagerBinary GetInstance()
    {
        if (!m_instance)
            m_instance = FindObjectOfType<MCLInputManagerBinary>();

        return m_instance;
    }

    static MCLInputManagerBinary m_instance;

    [SerializeField] MCLBinaryInputData[] m_binaryInputs;

    [Header("[Mobile/Screenspace controls binding")]
    [SerializeField] MCLBinaryInputButton[] m_binaryInputButtons;



    private void Start()
    {
    TryInit_BinaryInputButtons();
    }




    public MCLBinaryInputData GetInputType(string p_inputDescription)
    {
        for (int i = 0; i < m_binaryInputs.Length; i++)
        {
            if (m_binaryInputs[i].inputDescription == p_inputDescription)
            return m_binaryInputs[i];
        }

        return null;
    }


    void TryInit_BinaryInputButtons()
    {
    
        for(int i =0; i < m_binaryInputButtons.Length; i++)
        {
            m_binaryInputButtons[i].TryInitWith(GetInputType(m_binaryInputButtons[i].inputPairDescription));
        }
    }

    void CheckFor_KeyUp()
    {
        for (int i = 0; i < m_binaryInputs.Length; i++)
        {
            m_binaryInputs[i].OnBinaryInputUp();
        }
    }

    void CheckForKeyDown()
    {
        for (int i = 0; i < m_binaryInputs.Length; i++)
        {
            m_binaryInputs[i].OnBinaryInputDown();
        }
    }
    private void Update()
    {
        CheckForKeyDown();
        CheckFor_KeyUp();
    }
}
