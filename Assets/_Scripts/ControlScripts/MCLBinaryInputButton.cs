﻿using UnityEngine;
using UnityEngine.EventSystems;


[System.Serializable]
public class MCLBinaryInputButton
{
    [SerializeField] string m_inputPairDescription;
    public string inputPairDescription
    {
        get
        {
            return m_inputPairDescription;
        }
    }


    [SerializeField] EventTrigger m_eventTriggerUIObject;

    MCLBinaryInputData m_pairedData;

    public void TryInitWith(MCLBinaryInputData p_pairedData)
    {
        m_pairedData = p_pairedData;

        if (m_pairedData == null)
            return;


        EventTrigger.Entry inputDownTrigger = new EventTrigger.Entry();
        inputDownTrigger.eventID = EventTriggerType.PointerDown;
        inputDownTrigger.callback.AddListener((data) => { OnPairedPress((PointerEventData)data); });
        m_eventTriggerUIObject.triggers.Add(inputDownTrigger);

        EventTrigger.Entry inputUpTrigger = new EventTrigger.Entry();
        inputUpTrigger.eventID = EventTriggerType.PointerExit;
        inputUpTrigger.callback.AddListener((data) => { OnPairedRelease((PointerEventData)data); });
        m_eventTriggerUIObject.triggers.Add(inputUpTrigger);

        // Debug.Log("[BinaryInputButton Init Success " + m_inputPairDescription);
    }


    void OnPairedPress(PointerEventData p_data)
    {

        // Debug.Log("[PairedPress] " + m_inputPairDescription);
        if (m_pairedData == null)
            return;

        m_pairedData.OnUIInputDown();
    }

    void OnPairedRelease(PointerEventData p_data)
    {
        if (m_pairedData == null)
            return;

        m_pairedData.OnUIInputUp();
    }
}
