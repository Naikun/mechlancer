﻿using UnityEngine;

public enum LookMethod
{
    Joystick,
    MouseLook
};


public enum BinaryInputMethod
{
    KeyCode,
        Mousebutton
};
public enum DirectionalInputMethod
{
    Joystick,
    Mouse,
    KeyboardAxis
};