﻿using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] Transform m_toFollow;
    [SerializeField] Rigidbody m_optionalRGDBDY;
    [SerializeField] float m_optionalVelocityMod = 1;

    [SerializeField] float m_followRate = 1;
    [SerializeField] Vector3 m_offset;
    [SerializeField] Vector3 m_offsetMax;


    [SerializeField] Vector3 m_floorPosition;



    Vector3 GetSLerpedOffset(float p_lerpVal)
    {
        return Vector3.Lerp(m_offset, m_offsetMax, p_lerpVal);
    }


    Vector3 GetDesiredPos()
    {

        if (m_optionalRGDBDY == null)
            return m_toFollow.position + m_offset;

        else
            return (m_toFollow.position + m_offset) + (m_optionalRGDBDY.velocity * m_optionalVelocityMod);
    }


    void TryMoveCamera()
    {
        transform.position = Vector3.Lerp(transform.position, GetDesiredPos(), Time.deltaTime * m_followRate);
    }

    private void FixedUpdate()
    {
        if (m_toFollow == null)
            return;

        TryMoveCamera();
    }

}
